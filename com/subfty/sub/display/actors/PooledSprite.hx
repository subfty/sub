package com.subfty.sub.display.actors;

import flash.events.Event;
import flash.display.Sprite;

class PooledSprite extends Sprite{

    public var active     : Bool;
    public var renderable : Bool;

    public function new() : Void{
        super();
        kill();

        addEventListener(Event.ENTER_FRAME, _enterFrame);
    }

    function _start() : Void{
        active = true;
        renderable = false;
    }

    public function kill() : Void{
        renderable = false;
        active     = false;
    }

    @:noCompletnion
    function _enterFrame(e : Event){
        if (!active) return;

        enterFrame();
        renderable = true;
    }

    function enterFrame(){
        throw("Did you forgot to implement enterFrame function?");
    }
}