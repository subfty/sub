package com.subfty.sub.display.actors;

import com.subfty.sub.display.RGBColor;

@:publicFields
class SpriteData{
    var x       : Float;
    var y       : Float;
    var w       : Float;
    var h       : Float;
    var alpha   : Float;
    var color   : RGBColor;
    var visible : Bool;
    var rot     : Float;
    var scaleX  : Float;
    var scaleY  : Float;

    function new(){
        color = new RGBColor();
        rot = 0.0;
        scaleX = 1.0;
        scaleY = 1.0;
    }

    public function reset(){
        x = 0;
        y = 0;
        w = 0;
        h = 0;
        alpha = 1.0;
        color.set(1.0, 1.0, 1.0);
        rot = 0.0;
        scaleY = scaleY = 1.0;
        visible = true;
    }

    public function setRect(x : Float, y : Float, w : Float, h : Float ){
        this.visible = true;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public function setScale(scaleX : Float, scaleY : Float){
        this.visible = true;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }
}