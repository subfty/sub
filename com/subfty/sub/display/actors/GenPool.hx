package com.subfty.sub.display.actors;

import flash.display.DisplayObject;
import flash.display.Sprite;

class GenPool <T : ( {public function kill()  : Void;
                      public var active       : Bool;
                      public var renderable   : Bool;},
                      DisplayObject) >
            extends Sprite{

    public var objects : Sprite;
    var _generate : Void -> T;

    public function new(generate : Void -> T){
        super();

        _generate = generate;
        objects = new Sprite();
        this.addChild(objects);
    }

    public function obtain() : T {
        var newObject : T = null;

        for (i in 0 ... objects.numChildren){
            var o : T = cast(objects.getChildAt(i));
            if (!o.active) {
                newObject = o;
                break;
            }
        }

        if (newObject == null){
            newObject = _generate();
            objects.addChild(newObject);
        }

        return newObject;
    }

    public function freeAll() {
        mapForActive(function(o : T){ o.kill(); });
    }

    public function numActive(){
        var c : Int = 0;
        mapForActive(function(o : T){ c++; });
        return c;
    }

    public function bringToFront(object : T){
        objects.removeChild(object);
        objects.addChild(object);
    }

  //LAMBDAS
    public function mapForActive(act : T -> Void) {
        for (i in 0 ... objects.numChildren){
            var o : T = cast(objects.getChildAt(i));
            if (o.active)
                act(o);
        }
    }

    public function mapForRenderable(act : T -> Void) {
        for (i in 0 ... objects.numChildren){
            var o : T = cast(objects.getChildAt(i));
            if (o.active && o.renderable)
                act(o);
        }
    }
}