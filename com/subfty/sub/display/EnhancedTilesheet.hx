package com.subfty.sub.display;

import flash.geom.Point;
import flash.geom.Rectangle;
import Reflect;
import openfl.Assets;
import openfl.display.Tilesheet;
import flash.display.Graphics;
import com.subfty.sub.display.actors.SpriteData;
import flash.display.BitmapData;

class EnhancedTilesheet extends Tilesheet implements Dynamic<Int>{

    private static var FLAGS = Tilesheet.TILE_ALPHA | Tilesheet.TILE_RGB | Tilesheet.TILE_TRANS_2x2;

    var _tDataArr   : Array < Float >;
    public var _originSize : Array < {w : Float, h : Float} >;
    public var _originPos  : Array < {x : Float, y : Float} >;

    public function new( path : String ){
        _tDataArr   = [];
        _originSize = [];
        _originPos = [];

        var xml : Xml = Xml.parse(Assets.getText("img/"+path+"/"+path+".xml"));

        for( atlas in xml.elementsNamed("TextureAtlas")){
            super( Assets.getBitmapData("img/"+path+"/"+atlas.get("imagePath")) );

            for( sprite in atlas.elementsNamed("SubTexture")){
                var x : Float = Std.parseFloat(sprite.get("x"));
                var y : Float = Std.parseFloat(sprite.get("y"));
                var w : Float = Std.parseFloat(sprite.get("width"));
                var h : Float = Std.parseFloat(sprite.get("height"));

                var id : Int = addTileRect(new Rectangle(x, y, w, h), new Point(w / 2, h / 2));
                _originSize[id] = {w : w, h : h};
                _originPos[id]  = {x : x, y : y};
                Reflect.setField(this, sprite.get("name"), id);
            }

            break;
        }
    }

    public function addSprite(spriteId : Int,
                              x : Float, y : Float, w : Float, h : Float, // rect
                              rot : Float = 0,
                              r : Float = 1, g : Float = 1, b : Float = 1, a : Float = 1, // tint
                              scaleX : Float = 1, scaleY : Float = 1 //scale
                              ){

        _tDataArr.push(x);
        _tDataArr.push(y);

        _tDataArr.push(spriteId);

        var tScaleX : Float = w / _originSize[spriteId].w * scaleX;
        var tScaleY : Float = h / _originSize[spriteId].h * scaleY;
        var cos    : Float = Math.cos(rot);
        var sin    : Float = Math.sin(rot);

        _tDataArr.push(  cos * tScaleX );
        _tDataArr.push(  sin * tScaleX );
        _tDataArr.push(  -sin * tScaleY );
        _tDataArr.push(  cos * tScaleY );

        _tDataArr.push(r);
        _tDataArr.push(g);
        _tDataArr.push(b);
        _tDataArr.push(a);
    }

    public function getBitmapData() : BitmapData{
        #if js
        return nmeBitmap;
        #else
        return __bitmap;
        #end
    }

    public function addSpriteData(spriteId : Int, spriteD : SpriteData){
        addSprite(spriteId, spriteD.x, spriteD.y, spriteD.w, spriteD.h, spriteD.rot,
                  spriteD.color.r, spriteD.color.g, spriteD.color.b, spriteD.alpha,
                  spriteD.scaleX, spriteD.scaleY);
    }

    public inline function getWidthMatchingHeight( spriteId : Int, height : Float ) : Float {
        return _originSize[spriteId].w / _originSize[spriteId].h * height;
    }

    public inline function getHeightMatchingWidth( spriteId : Int, width : Float ) : Float {
        return _originSize[spriteId].h / _originSize[spriteId].w * width;
    }

    public function render(g : Graphics){
        g.clear();
        drawTiles(g, _tDataArr, true, FLAGS);
        while(_tDataArr.length > 0)
            _tDataArr.pop();
    }
}