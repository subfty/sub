package com.subfty.sub.display;

import flash.events.Event;
import de.polygonal.ds.LinkedQueue;
import flash.display.Sprite;
import com.subfty.sub.svg.SVGParser;

/**
 * ...
 * @author Filip Loster
 */

class Screen extends Sprite {

    public function new(parent : SMain, name : String, ?noParse : Bool = false) {
        super();

        this.visible = false;
        this.name = name;

        parent.addChildAt(this, 0);
        SMain.screens.set(this.name, this);

        if(!noParse)
            SVGParser.parse("layouts/" + name + ".svg", this);

        this.addEventListener(Event.ENTER_FRAME, _enterFrame);
    }

  //CALLBACKS
    /**
	 * called when this screen is set as an active
	 */
    public function load() {}

    /**
     * called when this screen is setted to be active in background of another screen.
     **/
    public function loadInBackground(){
        load();
    }

    public function postLoad() {}

    /**                                sasasasaaw
	 * called when screen is set as inactive
	 */
    public function unload() {}

    /**
	 * called when application is paused/minimised
	 */
    public function pause() { }

    /**
	 * called when application is resumed
	 **/
    public function resume() { }

    public function onResize(){}

    @:noCompletnion
    function _enterFrame(e){
        if(!this.visible) return;
        enterFrame();
    }

    function enterFrame(){
    }

    public function requieScreenInBackgrund() : Screen {
        return null;
    }
}