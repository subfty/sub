package com.subfty.sub.display;

import motion.actuators.GenericActuator.IGenericActuator;
import motion.easing.Linear;
import motion.easing.IEasing;
import motion.Actuate;
import com.subfty.sub.display.RGBColor;
@:publicFields
class RGBColor {

    var intVal (get_intVal, null) : Int;
    function get_intVal() : Int{
        return (Math.round(r * 255.0) << 16) +
               (Math.round(g * 255.0) << 8)  +
               (Math.round(b * 255.0));
    }

    var r : Float;
    var g : Float;
    var b : Float;
    var a : Float;

    public function new(toCopy : RGBColor = null, intVal : Int = -1, r : Float = 1.0, g : Float = 1.0, b : Float = 1.0){
        if(toCopy != null){
            set(toCopy);
        }else if(intVal >= 0){
            set(intVal);
        }else{
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = 1.0;
        }
    }

    public function set(rgbc : RGBColor = null, intVal : Int = -1, r : Float = 1.0, g : Float = 1.0, b : Float = 1.0, a : Float = -1.0){
        if(rgbc != null){
            this.r = rgbc.r;
            this.g = rgbc.g;
            this.b = rgbc.b;
            this.a = rgbc.a;
        }else if(intVal >= 0 ){
            this.r = (intVal >> 16) / 255.0;
            this.g = ((intVal >> 8) % 256) / 255.0;
            this.b = (intVal % 256) / 255.0;

            this.a = 1.0;
        }else{
            this.r = r;
            this.g = g;
            this.b = b;
            if(a >= 0)
                this.a = a;
            else
                this.a = 1.0;
        }
    }

    public function tweenTo(duration : Float, target : RGBColor, ?ease : IEasing = null) : IGenericActuator{
        if(ease == null)
            return Actuate.tween(this, duration, {r : target.r, g : target.g, b : target.b});
        return Actuate.tween(this, duration, {r : target.r, g : target.g, b : target.b})
                      .ease(ease);
    }

    public static function Init(){
        if(tmpInstance == null)
            tmpInstance = new RGBColor();
    }

    private static var tmpInstance : RGBColor;
    public static inline function getTmpInstance() : RGBColor {
        return tmpInstance;
    }
}