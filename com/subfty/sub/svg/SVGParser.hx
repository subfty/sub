package com.subfty.sub.svg;

import openfl.Assets;
import com.subfty.sub.svg.actors.SVGRect;
import flash.geom.Matrix;
import flash.display.Sprite;
import com.subfty.sub.SMain;
import com.subfty.sub.helpers.Geom;
import com.subfty.sub.helpers.Statics;
import com.subfty.sub.svg.actors.Trigger;
import com.subfty.sub.svg.actors.SVGImages;
import com.subfty.sub.svg.actors.GraphicTrigger;
import com.subfty.sub.svg.actors.ActorPxTextField;

/**
 * @author Filip Loster
 */

class SVGParser{
    static var aliases : Map<String, String>;

    private static var mTranslateMatch = ~/translate\((.*)[, ](.*)\)/;
    private static var mScaleMatch = ~/scale\((.*)\)/;
    private static var mMatrixMatch = ~/matrix\((.*)[, ](.*)[, ](.*)[, ](.*)[, ](.*)[, ](.*)\)/;
    private static var mURLMatch = ~/url\(#(.*)\)/;

  //DEFAULT SVG PARSER FUNCTIONS
    static var svgImages : SVGImages;
    public static var parseHash : Map<String, Sprite -> Xml -> Void>;

    public static function parseImageTag(root : Sprite, e : Xml){
        svgImages.addImage(e);
    }

    public static function parseRectTag(root : Sprite, e : Xml){
        root.addChild(new SVGRect(e));
    }

    public static function parseTextTag(root : Sprite, e : Xml){
        trace("Parsing text is not supported (right now)!");
        /*trace("parsing text: ");
        var textValue:String = "";
        var t_x:Float = 0;
        var t_y:Float = 0;
        var t_w:Float = 0;
        var t_h:Float = 0;
        var translateM:Matrix = getTransformMatrix(el);

        for (child in el){
            trace("    nodeName: " + child.nodeName);
            if (child.nodeName == "svg:flowPara") {
                trace("    flowPara: " + child.firstChild().toString());
            }else if (child.nodeName == "svg:flowRegion") {
                var rect:Xml = child.firstChild();


                trace("    rect x: " + rect.get("x") + " y: " + rect.get("y") +
                      " width: "+rect.get("width")+" height: " +rect.get("height"));
            }
        }*/
    }

    public static function parse(file : String, root : Sprite, ?noGroupNesting : Bool = false, ?skipGroupNames : Array<String> = null) {
        if(parseHash == null)
           parseHash = ["image" => parseImageTag,
                        "rect" => parseRectTag,
                        "flowRoot" => parseTextTag];
        if (aliases == null)
            loadAliases();

        var inXML:Xml = Xml.parse(Assets.getText(file));
        var svg = inXML.firstElement();

        if (svg == null || (svg.nodeName != "svg" && svg.nodeName != "svg:svg"))
            throw "Not an SVG file (" + (svg == null ? "null" : svg.nodeName) + ")";
        loadGroup(root, svg, noGroupNesting, skipGroupNames);
      //CLEARING STUFF
        Trigger.clearActionHash();
    }

    static function loadGroup (g : Sprite, inG : Xml, ?noGroupNesting : Bool = false, ?skipGroupNames : Array<String> = null) : Sprite {
        svgImages = new SVGImages();

        for (el in inG.elements ()) {
            var name = el.nodeName;
            var classPatch : String = el.get("class");

            if (name.substr (0, 4) == "svg:")
                name = name.substr(4);

            if(el.get("ignore") == "true")
                continue;

            if (name == "g") {
                var skip : Bool = false;
                if(skipGroupNames != null){
                    var name : String = el.get("id");
                    if(name != null)
                        for(m in skipGroupNames)
                            if(m == name){
                                skip = true;
                                break;
                            }
                }

                if(!skip){
                    var s : Sprite = new Sprite();

                    if(noGroupNesting)
                        s = g;
                    else{
                        g.addChild(s);
                        g.getChildAt(g.numChildren - 1).name = el.get("id");
                        applyTransformToPoint(el, cast(g.getChildAt(g.numChildren - 1)));
                    }
                    loadGroup(s, el);
                }
            } else if (classPatch != null) {
              //CREATING NEW OBJECT IF ASSOCIATED WITH THIS RECTANGLE
                if (aliases.exists(classPatch))
                    classPatch = aliases.get(classPatch);
                var obj : Dynamic = Type.createInstance(Type.resolveClass(classPatch), [el]);
                //obj.name = el.get("id");
                g.addChild(cast(obj));
                g.getChildAt(g.numChildren - 1).name = el.get("id");
            }else
                if(parseHash.exists(name)){
                    parseHash.get(name)(g, el);
                }
        }

        if(svgImages.images.length > 0){
            g.addChild(svgImages);
        }
        return g;
    }

//WORKERS
    private static function getFloat (inXML : Xml, inName : String, inDef : Float = 0.0) : Float {
        if (inXML.exists (inName))
            return Std.parseFloat (inXML.get (inName));

        return inDef;
    }

    public static function getTransformMatrix(xml : Xml) : Matrix {
        if(xml.exists("transform")){
            var m:Matrix = new Matrix(0, 0, 0, 0, 0, 0);
            m.identity();
            applyTransform(m, xml.get("transform"));
            return m;
        }
        return null;
    }

    public static function applyTransform (ioMatrix : Matrix, inTrans : String) : Float {
        var scale = 1.0;

        if (mTranslateMatch.match(inTrans)){
          // TODO: Pre-translate

            ioMatrix.translate (Std.parseFloat (mTranslateMatch.matched (1)), Std.parseFloat (mTranslateMatch.matched (2)));

        } else if (mScaleMatch.match (inTrans)) {
          // TODO: Pre-scale

            var str : Array<String> = mScaleMatch.matched(1).split(",");
            ioMatrix.scale (Std.parseFloat (str[0]),
            Std.parseFloat (str[1]));
            scale = Std.parseFloat (str[0]);

        } else if (mMatrixMatch.match (inTrans)) {

            var m = new Matrix (
                Std.parseFloat (mMatrixMatch.matched (1)),
                Std.parseFloat (mMatrixMatch.matched (2)),
                Std.parseFloat (mMatrixMatch.matched (3)),
                Std.parseFloat (mMatrixMatch.matched (4)),
                Std.parseFloat (mMatrixMatch.matched (5)),
                Std.parseFloat (mMatrixMatch.matched (6))
            );

            m.concat (ioMatrix);

            ioMatrix.a = m.a;
            ioMatrix.b = m.b;
            ioMatrix.c = m.c;
            ioMatrix.d = m.d;
            ioMatrix.tx = m.tx;
            ioMatrix.ty = m.ty;

            scale = Math.sqrt (ioMatrix.a * ioMatrix.a + ioMatrix.c * ioMatrix.c);

        } else
            trace("Warning, unknown transform:" + inTrans);

        return scale;
    }

    //UTILS
    /**
	     * List of additional parameters:
		 * fillScreen: true, width, height
		 * stickToBorderY: top, bottom
		 * stickToBorderX: left, right
	 */
    public static function applyTransformToRect(xml : Xml,
                                                rect : { x : Float, y : Float, w : Float, h : Float }) : Matrix {
        var m : Matrix = applyTransformToPoint(xml, cast(rect));

        rect.w = Std.parseFloat(xml.get("width"));
        rect.h = Std.parseFloat(xml.get("height"));

        if (m != null) {
            rect.w = rect.w * m.a;
            rect.h = rect.h * m.d;
        }

      //PARSING TAG FILL SCREEN
        if (xml.exists("fillScreen")) {
            var fs:String = xml.get("fillScreen");
            if(fs == "true") {
                rect.x = -(SMain.SCREEN_W - SMain.STAGE_W) / 2;
                rect.y = -(SMain.SCREEN_H - SMain.STAGE_H) / 2;
                rect.w = SMain.SCREEN_W;
                rect.h = SMain.SCREEN_H;
            }else if (fs == "width") {
                rect.x = -(SMain.SCREEN_W - SMain.STAGE_W) / 2;
                rect.w = SMain.SCREEN_W;
            }else if (fs == "height") {
                rect.y = -(SMain.SCREEN_H - SMain.STAGE_H) / 2;
                rect.h = SMain.SCREEN_H;
            }else
                trace("corrupted fillScreen value");
        }

      //PARSING TAG STICK TO BORDER Y
        if (xml.exists("stickToBorderY")) {
            var fs:String = xml.get("stickToBorderY");
            if (fs == "top") {
                rect.y = SMain.STAGE_H + (SMain.SCREEN_H - SMain.STAGE_H) / 2 - rect.h;
            }else if (fs == "bottom") {
                rect.y = -(SMain.SCREEN_H - SMain.STAGE_H) / 2;
            }else
                trace("corrupted stickToBorderY value");
        }

      //PARSING TAG STICK TO BORDER X
        if (xml.exists("stickToBorderX")) {
            var fs:String = xml.get("stickToBorderX");
            if (fs == "left") {
                rect.x = -(SMain.SCREEN_W - SMain.STAGE_W) / 2;
            }else if (fs == "right") {
                rect.x = SMain.STAGE_W + (SMain.SCREEN_W - SMain.STAGE_W) / 2 - rect.w;
            }else
                trace("corrupted stickToBorderX value");
        }

        return m;
    }
    public static function applyTransformToPoint(xml : Xml,
                                                 point : { x : Float, y : Float }) : Matrix {

        var m : Matrix = SVGParser.getTransformMatrix(xml);

        point.x = 0;
        point.y = 0;

        if(xml.exists("x"))
            point.x = Std.parseFloat(xml.get("x"));
        if(xml.exists("y"))
            point.y = Std.parseFloat(xml.get("y"));

        if (m != null) {
            Statics.p1.x = point.x;
            Statics.p1.y = point.y;
            Geom.multiplyPointByMatrix(m, Statics.p1);

            point.x = Statics.p1.x;
            point.y = Statics.p1.y;
        }
        return m;
    }

    public static function getColor(xml : Xml) : Int {
        return Std.parseInt("0x"+getParam(xml, "style", "fill:#"));
    }

    public static function getAlpha(xml : Xml) : Float {
        return Std.parseFloat(getParam(xml, "style", "fill-opacity:"));
    }

    private static function getParam(xml:Xml, att:String, tag:String):String {
        if (xml.exists(att)) {
            var attval : String = xml.get(att);
            var start  : Int = attval.indexOf(tag) + tag.length;
            var end    : Int = attval.indexOf(";", start);
            if(end == -1)
                end = attval.length;
            return attval.substr(start, end - start);
        }
        return "";
    }

    static function loadAliases() {
        aliases = new Map<String, String>();

        var xml:Xml = Xml.parse(Assets.getText("data/aliases.xml")).firstChild();
        for (el in xml.elements ())
            aliases.set(el.get("id"), el.get("value"));
    }
}