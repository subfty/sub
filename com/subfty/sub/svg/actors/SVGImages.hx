package com.subfty.sub.svg.actors;

import com.subfty.sub.svg.actors.SVGRect.SRect;
import com.subfty.sub.svg.actors.Trigger;
import flash.events.TouchEvent;
import flash.geom.Rectangle;
import flash.events.MouseEvent;
import flash.events.Event;
import flash.display.Sprite;
import com.subfty.sub.SMain;

class SVGImages extends Sprite{

    public var images : Array< {id : String,
                                arr : Array<String>,
                                r   : SRect,
                                add : {rot : Float, r : Float, g : Float, b : Float, alpha : Float}} >;
    var rectActions : Array< {rect : Rectangle,
                              goToScreen : String,
                              xml : Xml,
                              action : Xml->Void}>;

    public function new(){
        super();

        images = [];
        rectActions = [];

        this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
        #if mobile
            this.addEventListener(TouchEvent.TOUCH_BEGIN, onTap);
        #else
            this.addEventListener(MouseEvent.MOUSE_DOWN, onTap);
        #end
    }

    public function addImage(xml : Xml){

        var r : SRect = {x : 0.0, y : 0.0, w : 0.0, h : 0.0};
        SVGParser.applyTransformToRect(xml, r);

        var href  : String = xml.get("xlink:href");
        var image : String = href.substring(href.lastIndexOf("/") + 1, href.lastIndexOf("."));
        href = href.substring(0, href.lastIndexOf("/"));
        var atlas : String = href.substring(href.lastIndexOf("/") + 1);
        atlas = "ts" + atlas.charAt(0).toUpperCase() + atlas.substring(1);

        images.push({id  : xml.get("id"),
                     arr : [atlas, image],
                     r   : r,
                     add : {rot : 0.0, r : 1.0, g : 1.0, b : 1.0, alpha : 1.0}});

        var goToScreen : String = xml.get("goToScreen");
        var a : String = xml.get("action");
        var action : Xml -> Void = if (a != null && Trigger.actions.exists(a)) Trigger.actions.get(a) else null;

        if(goToScreen != null || action != null){
            rectActions.push({rect       : new Rectangle(r.x, r.y, r.w, r.h),
                              xml        : xml,
                              goToScreen : xml.get("goToScreen"),
                              action     : action});
        }
    }

    function onEnterFrame(e){
        if(images.length > 0){
            graphics.clear();

            var prevAtlas : Dynamic = Reflect.field(SMain.artRef, images[0].arr[0]);
            for(img in images){
                var atlas : Dynamic = Reflect.field(SMain.artRef, img.arr[0]);
                atlas.addSprite(Reflect.field(atlas, img.arr[1]),
                                img.r.x + img.r.w / 2, img.r.y + img.r.h / 2, img.r.w, img.r.h,
                                img.add.rot, img.add.r, img.add.g, img.add.b, img.add.alpha);

                if(prevAtlas != atlas){
                    atlas.render(graphics);
                    prevAtlas = atlas;
                }
            }

            prevAtlas.render(graphics);
        }
    }

    public function getImage(id : String){
        for(i in images)
            if(i.id == id)
                return i;
        return null;
    }

    function onTap(e: { localX : Float, localY : Float, target : Dynamic}) {
        if (e.target != this) return;

        for(i in 0 ... rectActions.length){
            var t = rectActions[rectActions.length - i - 1];
            if(t.rect.contains(e.localX, e.localY)){
                if(t.action != null)
                    t.action(t.xml);
                if(t.goToScreen != null)
                    SMain.setScreen(SMain.screens.get(t.goToScreen));

                return;
            }
        }
    }
}