package com.subfty.sub.svg.actors;

import flash.text.TextField;
import com.subfty.sub.data.Config;

/**
 * ...
 * @author Filip Loster
 */

@:keep
class SVGTextField extends TextField{

    public function new(xml:Xml) {
        super();
        Config.formatTextField(this, xml);
    }

}