package com.subfty.sub.svg.actors;

import flash.events.Event;
import flash.display.Sprite;
import com.subfty.sub.SMain;
import com.subfty.sub.svg.SVGParser;

typedef SRect = {
    x : Float,
    y : Float,
    w : Float,
    h : Float,
};

class SVGRect extends Sprite {

    var xml : Xml;

    public var w     : Float;
    public var h     : Float;

    public var colour : Int;

    public function new(xml : Xml){
        super();

        this.xml = xml;
        var r : SRect = {x : 0, y : 0, w : 0, h : 0};
        SVGParser.applyTransformToRect(xml, r);
        this.x = r.x;
        this.y = r.y;
        this.w = r.w;
        this.h = r.h;

        name = xml.get("id");

        if(x <= 0 && y <= 0 && w >= SMain.STAGE_W && h >= SMain.STAGE_H){
            x = -SMain.aspect.offsetX;
            y = -SMain.aspect.offsetY;
            w = SMain.STAGE_W + SMain.aspect.offsetX * 2;
            h = SMain.STAGE_H + SMain.aspect.offsetY * 2;
        }

        colour = SVGParser.getColor(xml);
        alpha =  SVGParser.getAlpha(xml);

        enterFrame(null);
    }

    public function addGraphicUpdater(){
        if(!hasEventListener(Event.ENTER_FRAME))
            addEventListener(Event.ENTER_FRAME, enterFrame);
    }

    function enterFrame(e){
        graphics.clear();
        graphics.beginFill(colour, alpha);
        graphics.drawRect(0, 0, w, h);
    }
}