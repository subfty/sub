package com.subfty.sub.svg.actors;

import com.subfty.sub.SMain;
import flash.events.Event;
import flash.display.Sprite;
import com.subfty.sub.display.bitmapFont.PxTextAlign;
import com.subfty.sub.display.bitmapFont.PxTextField;

#if (!flash && !js)
class ActorPxTextField extends PxTextField{

    public var w : Float;
    public var h : Float;

    var xml : Xml;

    public function new(xml : Xml){
        super(SMain.artRef.F_GAMESCORE);

        this.xml = xml;
        SVGParser.applyTransformToRect(xml, cast(this));
        this.color = 0xff0000;//SVGParser.getColor(xml);
        this.setWidth(Std.int(w));
        this.setScaleToMatchHeight(Std.int(h));
        this.text = xml.get("label");
        this.alpha = SVGParser.getAlpha(xml);

    }
}
#end