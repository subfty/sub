package com.subfty.sub.svg.actors;

import com.subfty.sub.svg.actors.SVGRect.SRect;
import flash.events.TouchEvent;
import flash.events.MouseEvent;
import flash.display.Sprite;
import com.subfty.sub.display.Screen;
import com.subfty.sub.svg.SVGParser;

/**
 * ...
 * @author Filip Loster
 */

@:keepSub
class Trigger extends Sprite{
//STATIC PART
    public static var actions : Map < String, Xml->Void > = new Map < String,  Xml->Void > ();

    public static function registarAction(id:String, func:Xml->Void) {
        if (!actions.exists(id))
            actions.set(id, func);
    }

    public static function clearActionHash() {
        actions = new Map < String, Xml->Void > ();
    }

  //VALUES
    public var w  : Float;
    public var h  : Float;
    public var id : String;

    var act        : Xml->Void = null;
    var goToScreen : String = null;
    var xml        : Xml;

    public function new(xml : Xml) {
        super();
        this.xml = xml;
        this.id = xml.get("id");

        var r : SRect = {x : 0.0, y : 0.0, w : 0.0, h : 0.0};
        SVGParser.applyTransformToRect(xml, r);
        this.x = r.x;
        this.y = r.y;
        this.w = r.w;
        this.h = r.h;

        var a : String = xml.get("action");
        if (a != null && actions.exists(a))
            act = actions.get(a);
        goToScreen = xml.get("goToScreen");

        #if mobile
			this.addEventListener(TouchEvent.TOUCH_BEGIN, onTap);
		#else
            this.addEventListener(MouseEvent.MOUSE_DOWN, onTap);
        #end
    }

//TOUCH EVENTS
    function onTap(e: { localX : Float, localY : Float, target : Dynamic}) {
        trace("tap? "+ this.id+" traget: "+e.target);
        trace("tapped: "+this.id);

        if (act != null)
            act(xml);
        if (goToScreen != null)
            SMain.setScreen(SMain.screens.get(goToScreen));
    }
}