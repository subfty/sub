package com.subfty.sub.data;

/**
 * Used for storing player progress
 * @author Filip Loster
 */

import flash.net.SharedObject;

/*
przyklad uzycia:

    GameState.so.data.dupa = 0;
    GameState.so.save();

    PROFIT!

 */

class GameState {

    public static var so : SharedObject;

    public static function Init(){
        if(so == null){
            so = SharedObject.getLocal("foam_1");
        }
    }

    public static function save(){
        /**#if ( cpp || neko )
            var flushStatus : SharedObjectFlushStatus = null;
        #else
            var flushStatus : String = null;
        #end*/

        try {
            //flushStatus =
            so.flush();
        } catch ( e:Dynamic ) {}

        //TODO: logowanie niepowodzenia tutaj
        /*if ( flushStatus != null )
            switch( flushStatus ) {
                case SharedObjectFlushStatus.PENDING:
                        //trace('requesting permission to save');
                case SharedObjectFlushStatus.FLUSHED:
                        //trace('value saved');
            }*/
    }
}
