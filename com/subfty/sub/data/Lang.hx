package com.subfty.sub.data;

import openfl.Assets;
import haxe.xml.Fast;

class Lang{
    public static var f;

    public static function Init() {
        //TODO: wykrywanie jezyka
        var path = "eng";

        try{
            var data = Assets.getText("data/languages/"+path+".xml");
            if(data != null){
                data = data.substr(data.indexOf("<lang"));

                f = new Fast(haxe.xml.Parser.parse(data).firstChild());
            }
        }catch(e : String){

        }
    }
}