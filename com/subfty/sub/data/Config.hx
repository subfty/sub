package com.subfty.sub.data;

import flash.display.Sprite;
import haxe.xml.Fast;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import com.subfty.sub.svg.SVGParser;

import openfl.Assets;
#if android
import openfl.utils.JNI;
#end


/**
 * Reads game configuration file
 * @author Filip Loster
 */
class Config{
    public static var f;

    public static function loadXmlData(path : String) {
        var data = Assets.getText("data/"+path+".xml");

        #if android
            #if !DONT_CACHE_CONFIG
			var getConfig = JNI.createStaticMethod("com/subfty/sub/nat/Nat", "getConfig", "(Ljava/lang/String;)Ljava/lang/String;");
			data = getConfig(data);
			#end
		#end

        data = data.substr(data.indexOf("<gdef"));

        f = new Fast(haxe.xml.Parser.parse(data).firstChild());
    }

    public static function getColor(x : Xml) : Int {
        return parseColor(Std.parseInt(x.get("r")),
                          Std.parseInt(x.get("g")),
                          Std.parseInt(x.get("b")));
    }

    public static function parseColor(r : Int, g : Int, b : Int) : Int {
        return (r << 16) + (g << 8) + b;
    }

    public static function setPos(x : Xml, target : Sprite) {
        target.x = Std.parseFloat(x.get("x"));
        target.y = Std.parseFloat(x.get("y"));
    }

    public static function formatTextField(textF : TextField, x : Xml, defFont : String = "_sans") : TextField{
        var font  : String = x.get("font"); if (font == null) font = defFont;
        var size  : Float = (x.get("size") == null ) ? 30 : Std.parseFloat(x.get("size"));
        var color : Int = getColor(x);
        var align : String = x.get("align"); if (align == null) align = "left"; else align = align.toLowerCase();

        var textFormat = new TextFormat(font, size, color);
        textF.width = (x.get("width") == null ) ? 100 : Std.parseFloat(x.get("width"));

        if (align == "left") textFormat.align = TextFormatAlign.LEFT;
        if (align == "right") textFormat.align = TextFormatAlign.RIGHT;
        if (align == "center") textFormat.align = TextFormatAlign.CENTER;
        if (align == "justify") textFormat.align = TextFormatAlign.JUSTIFY;

        textF.defaultTextFormat = textFormat;
        textF.setTextFormat(textFormat);
        textF.x = Std.parseFloat(x.get("x"));
        textF.y = Std.parseFloat(x.get("y"));
        textF.alpha = SVGParser.getAlpha(x);

        textF.mouseEnabled = false;
        textF.selectable = false;

        return textF;
    }
}