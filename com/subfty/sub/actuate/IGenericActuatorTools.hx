package com.subfty.sub.actuate;

import motion.actuators.GenericActuator;

@:access(motion.Actuate)
@:access(motion.actuators.GenericActuator)
@:access(motion.actuators.IGenericActuator)
class IGenericActuatorTools {

    public static function chain(m : IGenericActuator, tweens : Array<Dynamic>) : Dynamic {
        var longest : Dynamic = tweens[0];
        var me : Dynamic = m;
        for(t in tweens){
            t._delay += me._delay + me.duration;
            if(longest._delay + longest.duration < t._delay + t._duration)
                longest = t;
        }

        return longest;
    }
}