package com.subfty.sub.actuate;

import motion.actuators.GenericActuator;
import motion.Actuate;

@:access(motion.Actuate)
@:access(motion.actuators.GenericActuator)
class ActuateTools {
    public static function connectedTimer(object : Dynamic,
                                          duration : Float,
                                          customActuator : Class <GenericActuator> = null) : IGenericActuator{
        return Actuate.tween(object, duration, null, false, customActuator);
    }

    public static function bundle(tweens : Array<Dynamic>) : Dynamic {
        var longestTween : Dynamic = null;
        if(tweens.length > 0)
            longestTween = tweens[0];

        for(t in tweens)
            if(t.duration + t._delay > longestTween._delay + longestTween._delay)
                longestTween = t;

        return longestTween;
    }
}