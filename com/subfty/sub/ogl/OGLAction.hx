package com.subfty.sub.ogl;

import flash.geom.Rectangle;
import openfl.display.OpenGLView;

class OGLAction extends OpenGLView{
    var func : Rectangle -> Void;

    public function new ( func : Rectangle -> Void){
        super();
        this.func = func;
    }

    override public function render(rect : Rectangle){
        func(rect);
    }
}