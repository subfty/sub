package com.subfty.sub.ogl;

import flash.geom.Matrix3D;
import flash.geom.Rectangle;
import com.subfty.sub.ogl.ShaderManager;
import openfl.gl.GLBuffer;
import openfl.utils.Float32Array;
import openfl.gl.GL;
import openfl.gl.GLTexture;

class Rect3D {
    private function new(){}

    public static function getRenderRectFunc(shaderName : String)
            : Rectangle -> Matrix3D -> GLTexture -> Void {
        ShaderManager.get(shaderName);
        var buffer : GLBuffer = GL.createBuffer();
        var vertArr : Float32Array
            = new Float32Array(
                [   0,   0,  0,
                  1.0,   0,  0,
                    0, 1.0, 0,
                    0, 1.0, 0,
                  1.0,   0,  0,
                  1.0, 1.0, 0]);

        GL.bindBuffer(GL.ARRAY_BUFFER, buffer);
        GL.bufferData(GL.ARRAY_BUFFER, vertArr, GL.STATIC_DRAW);

        var renderFunc = function(rect : Rectangle, proj : Matrix3D, texture : GLTexture){
            GL.useProgram(ShaderManager.get(shaderName));

            GL.uniformMatrix3D (ShaderManager.getUniformL(shaderName, ShaderManager.PROJ_MAT),
                                false, proj);
            GL.uniform1f(ShaderManager.getUniformL(SHADER_NAME, ShaderManager.UFLOAT_1),
                         rect.width);
            GL.uniform1f(ShaderManager.getUniformL(SHADER_NAME, ShaderManager.UFLOAT_2),
                         rect.height);

            GL.bindBuffer(GL.ARRAY_BUFFER, buffer);

            GL.bindTexture(GL.TEXTURE_2D, texture);

            GL.vertexAttribPointer(ShaderManager.getAttributeL(SHADER_NAME, ShaderManager.VEC_POS_ATT),
                                   3, GL.FLOAT, false, 0, 0);
            GL.enableVertexAttribArray(ShaderManager.getAttributeL(SHADER_NAME, ShaderManager.VEC_POS_ATT));

            GL.drawArrays(GL.TRIANGLES, 0, 6);

            GL.disableVertexAttribArray(ShaderManager.getAttributeL(SHADER_NAME, ShaderManager.VEC_POS_ATT));

            GL.bindBuffer(GL.ARRAY_BUFFER, null);
        };

        return renderFunc;
    }

/*function getToProjection(near : Float, far : Float, fov : Float, aspectRatio : Float) : Array<Float>{
        var val = [1.0, 0.0, 0.0, 0.0,
                   0.0, 1.0, 0.0, 0.0,
                   0.0, 0.0, 1.0, 0.0,
                   0.0, 0.0, 0.0, 1.0];
        var l_fd : Float = (1.0 / Math.tan((fov * (Math.PI / 180.0)) / 2.0 / 360.0 * Math.PI * 2));
        var l_a1 : Float = (far + near) / (near - far);
        var l_a2 : Float = (2 * far * near) / (near - far);
        val[0] = l_fd / aspectRatio;
        val[1] = 0;
        val[2] = 0;
        val[3] = 0;
        val[4] = 0;
        val[5] = l_fd;
        val[6] = 0;
        val[7] = 0;
        val[8] = 0;
        val[9] = 0;
        val[10] = l_a1;
        val[11] = -1;
        val[12] = 0;
        val[13] = 0;
        val[14] = l_a2;
        val[15] = 0;
        return val;
    }       */
}