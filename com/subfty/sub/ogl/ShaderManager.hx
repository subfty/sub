package com.subfty.sub.ogl;

#if !flash
import openfl.gl.GLProgram;
import openfl.gl.GL;
import openfl.gl.GLShader;
import openfl.Assets;

typedef GLProgData = {
    var program    : GLProgram;
    var uniformL   : Map<String, Dynamic>;
    var attributeL : Map<String, Dynamic>;
};

class ShaderManager{
    //STANDARISED UNIFORM / ATTRIBUTE NAMES
    public static var POSITION ( default, null ) : String = "aPos";
    public static var RADIUS_ANGLE ( default, null ) : String = "radAng";
    public static var TEX_COORD_1 ( default, null ) : String = "aTexCoord";
    public static var TINT_C ( default, null ) : String = "tintC";
    public static var PROJ_MAT ( default, null ) : String = "projectionMatrix";
    public static var TEX_SAMPL_1 ( default, null ) : String = "uSampler";
    public static var UFLOAT_1 ( default, null ) : String = "uFloat1";
    public static var UFLOAT_2 ( default, null ) : String = "uFloat2";
    public static var UVEC4_1 ( default, null ) : String = "uVec4";
    public static var VEC_POS_ATT ( default, null) : String = "vec_pos";
    public static var V_COL (default, null) : String = "vCol";

    public static var AVEC3_1 (default, null) : String = "aVec3";
    public static var UVEC3_1 (default, null) : String = "uVec3";

    static var SHADER_DIR = "shaders/";
    static var SHADER_EXTENSION = ".glsl";
    static var VERT_TAG = "<vertex_shader/>";
    static var FRAG_TAG = "<fragment_shader/>";
    static var SHADER_RELOAD_TIME : Float = 2.0; //seconds

    static var _shaders : Map < String, GLProgData > = new Map< String, GLProgData >();
    static var _reloadDelay : Float = SHADER_RELOAD_TIME;

    public static function prepareShader(shaderName : String) {
        if(_shaders.exists(shaderName)) return;

        var source = extractShaderCodeFromFile(SHADER_DIR + shaderName + SHADER_EXTENSION);
        var program = createProgram(source.vertexS, source.fragmentS);


        _shaders.set( shaderName, { program    : program,
                      uniformL   : new Map<String, Dynamic> (),
                      attributeL : new Map<String, Dynamic> () } );
    }

    public static function get(shaderName : String) : GLProgram{
        if(_shaders.exists(shaderName))
            return _shaders.get(shaderName).program;

        prepareShader(shaderName);
        return get(shaderName);
    }

    public static function remove(shaderName : String) : Bool {
        if(!_shaders.exists(shaderName))
            return false;

        var program = _shaders.get(shaderName).program;

        var arr = GL.getAttachedShaders(program);
        for(s in arr){
            GL.detachShader(program, s);
            GL.deleteShader(s);
        }

        GL.deleteProgram(program);

        _shaders.remove(shaderName);
        return true;
    }

    public static function getUniformL(shaderName : String, varName : String) : Dynamic{
        return getLocation(shaderName, varName, 0);
    }

    public static function getAttributeL(shaderName : String, varName : String) : Dynamic{
        return getLocation(shaderName, varName, 1);
    }

    public static function update(delta : Float){
        _reloadDelay -= delta;
        if(_reloadDelay < 0){
            reloadAllShaders();
            _reloadDelay =  SHADER_RELOAD_TIME;
        }
    }

    public static function reloadAllShaders(){
        for(key in _shaders.keys()){
            var attribL  : Map<String, Dynamic> = _shaders.get(key).attributeL;
            var uniformL : Map<String, Dynamic> = _shaders.get(key).uniformL;

            remove(key);
            get(key);

          //RELOADING ATTRIBUTE LOCATIONS
            for(aKey in attribL)
                getAttributeL(key, aKey);
            for(uKey in uniformL)
                getUniformL(key, uKey);
        }
    }

    static function getLocation(shaderName : String, varName : String, func) : Dynamic{
        if(!_shaders.exists(shaderName))
            throw("shader "+shaderName+" does not exist");

        var value : Dynamic;
        if ( func == 0 ){
            if(_shaders.get(shaderName).uniformL.exists(varName))
                return _shaders.get(shaderName).uniformL.get(varName);

            value = GL.getUniformLocation(_shaders.get(shaderName).program, varName);
            _shaders.get(shaderName)
            .uniformL.set(varName, value);
        }else  {
            if(_shaders.get(shaderName).attributeL.exists(varName))
                return _shaders.get(shaderName).attributeL.get(varName);

            value = GL.getAttribLocation(_shaders.get(shaderName).program, varName);
            _shaders.get(shaderName)
            .attributeL.set(varName, value);
        }

        return value;
    }

    static function extractShaderCodeFromFile(path : String) : {vertexS : String, fragmentS : String} {
        var vs    : String = "";
        var fs    : String = "";
        var sauce : String = Assets.getText(path);

        var vtagPos : Int = sauce.indexOf(VERT_TAG);
        var ftagPos : Int = sauce.indexOf(FRAG_TAG);

        vs = sauce.substring(vtagPos + VERT_TAG.length, vtagPos > ftagPos ? sauce.length : ftagPos);
        fs = sauce.substring(ftagPos + FRAG_TAG.length, vtagPos > ftagPos ? vtagPos : sauce.length);

        return {vertexS : vs, fragmentS : fs};
    }

    static function createProgram(inVertexSource : String, inFragmentSource : String) : GLProgram{
        var prog = GL.createProgram();

        var vshader = createShader(inVertexSource, GL.VERTEX_SHADER);
        var fshader = createShader(inFragmentSource, GL.FRAGMENT_SHADER);

        GL.attachShader(prog, vshader);
        GL.attachShader(prog, fshader);
        GL.linkProgram(prog);

        if (GL.getProgramParameter(prog, GL.LINK_STATUS) == 0){
            var result = GL.getProgramInfoLog(prog);
            trace("error creating shader program");
            if (result!="")
                throw result;
        }

        return prog;
    }

    static function createShader(source : String, type : Int){
        var shader = GL.createShader(type);
        GL.shaderSource(shader, source);
        GL.compileShader(shader);
        if (GL.getShaderParameter(shader, GL.COMPILE_STATUS) == 0){
            trace("error creating shader");
            var err = GL.getShaderInfoLog(shader);
            if (err!="")
                throw err;
        }

        return shader;
    }
}

#end