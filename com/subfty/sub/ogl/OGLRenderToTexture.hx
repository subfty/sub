package com.subfty.sub.ogl;

import flash.geom.Rectangle;
import flash.display.Sprite;
import com.subfty.sub.ogl.OGLAction;
import openfl.gl.GLRenderbuffer;
import openfl.gl.GLTexture;
import openfl.gl.GLFramebuffer;
import openfl.gl.GL;

class OGLRenderToTexture {

    private function new(){}

    public static function attachCapture( doActionOnFramebuffer : GLTexture -> Void,
                                          target : Sprite,
                                          ?wrapBefore : Sprite = null,
                                          ?wrapAfter : Sprite = null){
        if(wrapBefore == null)
            wrapBefore = cast target.getChildAt(0);
        if(wrapAfter == null)
            wrapAfter = cast target.getChildAt(target.numChildren - 1);

        var framebuffer  : GLFramebuffer  = GL.createFramebuffer();
        var texture      : GLTexture      = GL.createTexture();
        var renderbuffer : GLRenderbuffer = GL.createRenderbuffer();

        var before : OGLAction = new OGLAction(function(rect : Rectangle){
            GL.viewport (Std.int (rect.x), Std.int (rect.y), Std.int (rect.width), Std.int (rect.height));
            GL.bindFramebuffer(GL.FRAMEBUFFER, framebuffer);
            GL.clearColor(0.0, 0.0, 0.0, 0.0);

            GL.clear(GL.COLOR_BUFFER_BIT);

          //TODO: rozwiaz problem przezroczystosci
            GL.enable(GL.BLEND);
            GL.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);
        });

        var after : OGLAction = new OGLAction(function(rect : Rectangle){
            GL.bindFramebuffer(GL.FRAMEBUFFER, null);
            doActionOnFramebuffer(texture);
        });

        GL.bindFramebuffer(GL.FRAMEBUFFER, framebuffer);
        GL.bindTexture(GL.TEXTURE_2D, texture);
        GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, SMain.aspect.screenW, SMain.aspect.screenH, 0, GL.RGBA, GL.UNSIGNED_BYTE, null);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);

        GL.framebufferTexture2D(GL.FRAMEBUFFER, GL.COLOR_ATTACHMENT0, GL.TEXTURE_2D, texture, 0);

        GL.bindFramebuffer(GL.FRAMEBUFFER, null);

        target.addChildAt(before, target.getChildIndex(wrapBefore));
        target.addChildAt(after, target.getChildIndex(wrapAfter));
    }
}