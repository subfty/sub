package com.subfty.sub.helpers;

import de.polygonal.core.math.random.Random;
import com.subfty.sub.SMain;

class TimeRandRange{
    public var minV : Float;
    public var maxV : Float;

    var gen : Float;
    public var weAreThere : Bool;

    public function new(?x : Xml, minV : Float = 1.0, maxV : Float = 1.0){
        if(x != null){
            this.minV = Std.parseFloat(x.get("minV"));
            this.maxV = Std.parseFloat(x.get("maxV"));
        }else{
            this.minV = minV;
            this.maxV = maxV;
        }

        initGenerator();
    }

    public inline function getRandV() : Float{
        return Random.frandRange(minV, maxV);
    }

    public function initGenerator(){
        gen = getRandV();
        weAreThere = false;
    }

    public function areWeThereYet() : Bool{
        gen -= SMain.delta / 1000.0;
        if(gen <= 0){
            gen = getRandV();
            weAreThere = true;
            return true;
        }
        return false;
    }
}