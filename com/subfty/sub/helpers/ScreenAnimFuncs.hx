package com.subfty.sub.helpers;

import com.subfty.sub.actuate.ActuateTools;
import com.subfty.sub.actuate.GenericActuatorTools;
import motion.easing.Sine;
import motion.Actuate;
import com.subfty.sub.display.Screen;
import flash.events.FullScreenEvent;

using com.subfty.sub.actuate.GenericActuatorTools;

class ScreenAnimFuncs{

    private function new(){}

    public static function stackPush(prev : Screen, next : Screen, callback : Dynamic){
        prev.scaleX = prev.scaleY = prev.alpha = 1.0;
        prev.x = prev.y = 0;

        next.scaleX = next.scaleY = 2.0;
        next.alpha = 0;
        next.x = -SMain.STAGE_W / 2;
        next.y = -SMain.STAGE_H / 2;

        Actuate.stop(prev, {scaleX: null, scaleY : null, x : null, y : null});
        Actuate.stop(next, {scaleX: null, scaleY : null, x : null, y : null});

        prev.mouseEnabled = prev.mouseChildren = false;
        next.mouseEnabled = next.mouseChildren = false;

        var arr = [Actuate.tween(prev, 0.3, {x : SMain.STAGE_W / 4, y : SMain.STAGE_H / 4, scaleX: 0.5, scaleY: 0.5, alpha : 0.0})
                          .ease(Sine.easeInOut),
                   Actuate.tween(next, 0.3, {x : 0, y : 0, scaleX: 1.0, scaleY: 1.0, alpha : 1.0})
                          .ease(Sine.easeInOut)
                          .delay(0.15)];

        if(prev.requieScreenInBackgrund() != null){
            Actuate.stop(prev.requieScreenInBackgrund(), {scaleX: null, scaleY : null, x : null, y : null});
            arr.push(Actuate.tween(prev.requieScreenInBackgrund(), 0.6,
                                   {x : 0, y : 0, scaleX: 1.0, scaleY: 1.0})
                            .ease(Sine.easeInOut));

        }

        ActuateTools.bundle(arr)
               .onComplete(function(){
                    prev.mouseEnabled = prev.mouseChildren = true;
                    next.mouseEnabled = next.mouseChildren = true;
                    callback();
                });
    }

    public static function stackPop(prev : Screen, next : Screen, callback : Dynamic){
        prev.scaleX = prev.scaleY = prev.alpha = 1.0;
        prev.x = prev.y = 0;

        next.scaleX = next.scaleY = 0.5;
        next.alpha = 0;
        next.x = SMain.STAGE_W / 4;
        next.y = SMain.STAGE_H / 4;

        Actuate.stop(prev, {scaleX: null, scaleY : null, x : null, y : null});
        Actuate.stop(next, {scaleX: null, scaleY : null, x : null, y : null});

        prev.mouseEnabled = prev.mouseChildren = false;
        next.mouseEnabled = next.mouseChildren = false;

        var arr = [Actuate.tween(prev, 0.3, {x : -SMain.STAGE_W / 2, y : -SMain.STAGE_H / 2, scaleX: 2.0, scaleY: 2.0, alpha : 0.0})
                          .ease(Sine.easeInOut),
                   Actuate.tween(next, 0.3, {x : 0, y : 0, scaleX: 1.0, scaleY: 1.0, alpha : 1.0})
                          .ease(Sine.easeIn)
                          .delay(0.15)];

        if(prev.requieScreenInBackgrund() != null){
            Actuate.stop(prev.requieScreenInBackgrund(), {scaleX: null, scaleY : null, x : null, y : null});
            arr.push(Actuate.tween(prev.requieScreenInBackgrund(), 0.6,
                                   {x : -SMain.STAGE_W / 22, y : -SMain.STAGE_H / 22, scaleX: 1.0 + 1.0 / 11.0, scaleY: 1.0 + 1.0 / 11.0})
                            .ease(Sine.easeInOut));
        }

        ActuateTools.bundle(arr)
               .onComplete(function(){
                   prev.mouseEnabled = prev.mouseChildren = true;
                   next.mouseEnabled = next.mouseChildren = true;
                   callback();
               });
    }

    public static function screenZoomIn(prev : Screen, next : Screen, callback : Dynamic){
        prev.scaleX = prev.scaleY = prev.alpha = 1.0;
        prev.x = prev.y = 0;

        Actuate.stop(prev, {scaleX: null, scaleY : null, x : null, y : null});

        prev.mouseEnabled = prev.mouseChildren = false;
        next.mouseEnabled = next.mouseChildren = false;

        Actuate.tween(prev, 0.3, {x : -SMain.STAGE_W / 2, y : -SMain.STAGE_H / 2,
                                                 scaleX: 2.0, scaleY: 2.0, alpha : 0.0})
               .ease(Sine.easeInOut)
               .onComplete(function(){
                   prev.mouseEnabled = prev.mouseChildren = true;
                   next.mouseEnabled = next.mouseChildren = true;
                   callback();
               });
    }
}