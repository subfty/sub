package com.subfty.sub.helpers;

import com.subfty.sub.ogl.ShaderManager;
import de.polygonal.core.math.Mathematics;
import flash.display.Sprite;
import flash.Lib;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.events.Event;

/**
 * ...
 * @author runnermark
 */

class FPS extends Sprite{
    var recordId : Int = 0;

    var fps_text : TextField;

    public var fps : Int;
    var prev       : Int;
    var times      : Array<Float>;

    var segmentTimes          : Array<Float>;
    var segNames              : Array<TextField>;
    var segColours            : Array<Int>;
    var segTPos               : Int;
    public var totalTime      : Int;
    var lastTime              : Int;

    public function new(inX:Float=10.0, inY:Float=10.0, inCol:Int = 0x000000){
        super();

        this.mouseEnabled = false;
        this.mouseChildren = false;

        segColours = [0x1B87E0, 0x1BE08E, 0xD61BE0, 0xE0DD1B, 0xE02F1B];
        segmentTimes = [];
        segNames = [];

        fps_text = new TextField();
        this.x = inX;
        this.y = inY;
        fps_text.selectable = false;
        fps_text.defaultTextFormat = new TextFormat("_sans", 20, inCol, true);
        fps_text.htmlText = "FPS:";
        fps_text.multiline = true;
        fps_text.mouseEnabled = false;
        fps_text.width = 150;
        this.addChild(fps_text);

        times = [];
        lastTime = 0;
        segTPos = 0;

        addEventListener(Event.ENTER_FRAME, onEnter);
    }

    public function addSegmentTime(time : Int, name : String, ?id = 0){
        if(id != recordId) return;

        if(segmentTimes.length == segTPos){
            var t : TextField = new TextField();
            t.x = 0; t.y = (segTPos + 1) * 8 +20;
            t.defaultTextFormat =new TextFormat("_sans", 8, 0xffffff, true);
            t.multiline = false;
            t.mouseEnabled = false;
            t.width = 200;
            t.selectable = true;
            this.addChild(t);

            segmentTimes.push(0);
            segNames.push(t);
        }

        segmentTimes[segTPos] += (time - segmentTimes[segTPos]) * 0.1;
        segNames[segTPos++].htmlText = name;
    }

    var now : Int;
    var id  : Int;

    public function startRecording(){
        now = Lib.getTimer();
    }

    public function endRecording(tag : String, ?id = 0){
        addSegmentTime(Lib.getTimer() - now, tag, id);
    }

    public function onEnter(e){

        var now = Lib.getTimer () / 1000;
        times.push(now);
        while(times[0]<now-1)
            times.shift();
        fps = times.length;

        if (visible && (prev != fps)){
            prev = fps;
            fps_text.htmlText = "FPS: " + fps;
        }

      //RENDERING TIMES
        totalTime = Lib.getTimer() - lastTime;
        lastTime = Lib.getTimer();
        var ttime : Int = totalTime;
        this.graphics.clear();
        for(t in segNames)
            t.visible = false;
        for(i in 1 ... segTPos + 1){
            graphics.beginFill(segColours[(i - 1) % segColours.length]);
            graphics.drawRect(15, 8 * i + 25, segmentTimes[i - 1] / totalTime * 200, 5);
            segNames[i - 1].visible = true;
            ttime -= Math.floor(segmentTimes[i - 1]);
        }
        graphics.beginFill(0xffffff);
        graphics.drawRect(15,  25, Math.max(0.0, ttime / totalTime) * 200, 5);

        segTPos = 0;
    }
}
