package com.subfty.sub.helpers;

import de.polygonal.core.math.random.Random;

class RandRange{
    public var minV : Int;
    public var maxV : Int;

    var gen : Int;
    public var weAreThere : Bool;

    public function new(?x : Xml, minV : Int = 0, maxV : Int = 0){
        if(x != null){
            this.minV = Std.parseInt(x.get("minV"));
            this.maxV = Std.parseInt(x.get("maxV"));
        }else{
            this.minV = minV;
            this.maxV = maxV;
        }

    }

    public inline function getRandV(){
        return Random.randRange(0, maxV - minV) + minV;
    }

    public function initGenerator(){
        gen = getRandV();
        weAreThere = false;
    }

    public function areWeThereYet() : Bool{
        if(gen-- <= 0){
            gen = getRandV();
            weAreThere = true;
            return true;
        }
        return false;
    }
}