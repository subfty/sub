package com.subfty.sub.utils;

import de.polygonal.core.math.random.Random;

@:publicFields
class ArrayTools{
    private function new(){

    }

    static function contain(arr : Array<Dynamic>, value : Dynamic) : Bool{
        for(a in 0 ... arr.length)
            if(arr[a] == value)
                return true;
        return false;
    }

    static function getRandomElement(arr : Array<Dynamic>) : Dynamic{
        if(arr.length == 0)
            return null;
        return arr[Random.randRange(0, arr.length - 1)];
    }

    static function last(arr : Array<Dynamic>) : Dynamic {
        if(arr.length == 0)
            return null;
        return arr[arr.length-1];
    }

    /**
     * Zajebane z polygonal.ds
    **/
    static function quickSort(a : Array<Dynamic>, first : Int, k : Int, cmp : Dynamic->Dynamic->Int){
        var last = first + k - 1;
        var lo = first;
        var hi = last;

        if (k > 1){
            var i0 = first;
            var i1 = i0 + (k >> 1);
            var i2 = i0 + k - 1;
            var t0 = a[i0];
            var t1 = a[i1];
            var t2 = a[i2];
            var mid:Int;
            var t = cmp(t0, t2);

            if (t < 0 && cmp(t0, t1) < 0)
                mid = cmp(t1, t2) < 0 ? i1 : i2;
            else{
                if (cmp(t1, t0) < 0 && cmp(t1, t2) < 0)
                    mid = t < 0 ? i0 : i2;
                else
                    mid = cmp(t2, t0) < 0 ? i1 : i0;
            }

            var pivot = a[mid];
            a[mid] = a[first];

            while (lo < hi){
                while (cmp(pivot, a[hi]) < 0 && lo < hi) hi--;
                if (hi != lo){
                    a[lo] = a[hi];
                    lo++;
                }

                while (cmp(pivot, a[lo]) > 0 && lo < hi) lo++;
                if (hi != lo){
                    a[hi] = a[lo];
                    hi--;
                }
            }

            a[lo] = pivot;
            quickSort(a, first, lo - first, cmp);
            quickSort(a, lo + 1, last - lo, cmp);
        }
    }

}