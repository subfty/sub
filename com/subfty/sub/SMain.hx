package com.subfty.sub;

import flash.events.MouseEvent;
import com.subfty.sub.display.RGBColor;
import com.subfty.sub.data.Lang;
#if !flash
import com.subfty.sub.ogl.ShaderManager;
#end
import flash.geom.Rectangle;
import com.subfty.sub.data.GameState;
import flash.display.StageScaleMode;
import flash.display.StageAlign;
import com.subfty.sub.data.Config;
import flash.events.Event;
import flash.Lib;
import com.subfty.sub.helpers.FixedAspectRatio;
import com.subfty.sub.display.Screen;
import flash.display.Sprite;
#if USE_OPENGL
import openfl.gl.GL;
import openfl.display.OpenGLView;
#end
#if (SHOW_FPS && debug)
import com.subfty.sub.helpers.FPS;
#end
#if USE_NAPE
import nape.shape.Shape;
import nape.space.Space;
import nape.geom.Vec2;
#end

using com.subfty.sub.utils.ArrayTools;

typedef Finger = {
    x : Float,
    y : Float,

    id   : Int,
    down : Bool,
};

class SMain extends Sprite {
  //ASPECT RATIO
  //SCREEN SIZE CLIPPED TO THE FIXED SCREEN PROPORTIONS
    public static var STAGE_W : Int = 0;
    public static var STAGE_H : Int = 0;

  //ACTUAL SCREEN SIZE
    public static var SCREEN_W(get_SCREEN_W, null) : Float;
    public static var SCREEN_H(get_SCREEN_H, null) : Float;

    public static var aspect : FixedAspectRatio;

    public static var artRef : Dynamic;
    public static var self : SMain;

  //SCREENS
    //current screen
    public static var screen(default, null) : Screen;
    static var backgroundScreen             : Screen;

    public static var screens : Map <String, Dynamic> = new Map <String, Dynamic> ();

    static function defAnimation(prevS : Screen, nextS : Screen, callback : Dynamic) : Void {
        nextS.scaleX = nextS.scaleY = 1.0;
        nextS.alpha = 1.0;
        nextS.x = nextS.y = 0;
        callback();
    }

    public static function setScreen(newScreen : Screen, ?animation : Screen -> Screen -> Dynamic -> Void = null){
        var prevScreen : Screen = SMain.screen;
        if(prevScreen == newScreen)
            return;

        if(animation == null)
            animation = defAnimation;

        if (prevScreen != null) {
            if(prevScreen.requieScreenInBackgrund() != null &&
               (newScreen == null || (prevScreen.requieScreenInBackgrund() != newScreen.requieScreenInBackgrund()))){
                prevScreen.requieScreenInBackgrund().visible = false;
                prevScreen.requieScreenInBackgrund().unload();
                backgroundScreen = null;
            }

            if(newScreen != null && newScreen.requieScreenInBackgrund() != null &&
               (prevScreen == null || (prevScreen.requieScreenInBackgrund() != newScreen.requieScreenInBackgrund()))){
                newScreen.requieScreenInBackgrund().loadInBackground();
                newScreen.requieScreenInBackgrund().visible = true;
                backgroundScreen = newScreen.requieScreenInBackgrund();
            }
        }else
            if(newScreen != null && newScreen.requieScreenInBackgrund() != null){
                newScreen.requieScreenInBackgrund().loadInBackground();
                newScreen.requieScreenInBackgrund().visible = true;
                backgroundScreen = newScreen.requieScreenInBackgrund();
            }

        if(backgroundScreen != null){
            var position : Int = -1;
            for(i in 0 ... self.numChildren)
                if(self.getChildAt(i) == backgroundScreen){
                    position = i;
                    break;
                }

            self.swapChildrenAt(0, position);
        }

        if(newScreen != null){
            newScreen.load();
            newScreen.visible = true;
        }

        animation(prevScreen, newScreen, function(){
            if(prevScreen != null){
                prevScreen.visible = false;
                prevScreen.unload();
            }

            SMain.screen = newScreen;
            SMain.screen.postLoad();
        });
    }

  //CALCULATING DELTA
    private static var prevFrame : Int = -1;
    public static var delta      : Int = 0;
    #if USE_NAPE
    public static var movAlpha  : Float = 0;
    /* clamped delta to prevent deathloop - use only when dealing with nape stuff */
    public static var napeDelta : Int;
    /* internal delta counter */
    static var nDelta           : Int;
    public static var desiredStepTime  : Int;

  //NAPE
    public static var VELOC_ITER          : Int;
    public static var DEATH_LOOP_MARGIN   : Int;
    public static var POS_ITER            : Int;
    var FIXED_TIMESTAMP     : Int;
    public static var space : Space;
    #end

  //DEBUG
    #if (SHOW_FPS && debug)
        public static var fps : FPS;
    #end

  //INITIATION
    public function new(art) {
        super();

        SMain.artRef = art;
        fingers = [];

        SMain.self = this;

        #if iphone
			Lib.current.stage.addEventListener(Event.RESIZE, init);
		#else
            addEventListener(Event.ADDED_TO_STAGE, init);
        #end
    }

    function init(e : Event) {
		#if iphone
		    //TODO: ogarnij na iphonie
            if (stageWidth < 960) { // iPhone classic
                stageWidth *= 2;
                stageHeight *= 2;
                Lib.current.scaleX = Lib.current.scaleY = 0.5;
            }
            if (stageWidth > 1024) { // iPad retina
                stageWidth = cast stageWidth / 2;
                stageHeight = cast stageHeight / 2;
                Lib.current.scaleX = Lib.current.scaleY = 2;
            }
		#end

        #if USE_OPENGL
        if(!OpenGLView.isSupported) throw("OpenGL View not supported");
        #end

      //SETTING CONSTANTS/LOADING DATA
        Config.loadXmlData("config");
        Lang.Init();
        GameState.Init();
        RGBColor.Init();
        SMain.artRef.loadBasicAssets();

      //BINDING INPUT EVENTS
        stage.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
        stage.addEventListener(MouseEvent.MOUSE_MOVE, onMove);
        stage.addEventListener(MouseEvent.MOUSE_UP,   onUp);

        STAGE_W = Std.parseInt(Config.f.att.width);
        STAGE_H = Std.parseInt(Config.f.att.height);

        aspect = new FixedAspectRatio(this, STAGE_W, STAGE_H);
        aspect.fix(null);

        backgroundScreen = null;

        #if USE_NAPE
      //LOADING NAPE DATA
        VELOC_ITER = Std.parseInt(Config.f.node.nape.node.accuracy.att.veloc_i);
		POS_ITER = Std.parseInt(Config.f.node.nape.node.accuracy.att.pos_i);

		FIXED_TIMESTAMP = Std.parseInt(Config.f.node.nape.att.fixed_timestamp);
		DEATH_LOOP_MARGIN = Std.parseInt(Config.f.node.nape.att.death_loop_margin);

      //NAPE INITIATION
        space = new Space(Vec2.weak(Std.parseFloat(Config.f.node.nape.node.gravity.att.x),
                                    Std.parseFloat(Config.f.node.nape.node.gravity.att.y)));
        nDelta = 0;
        desiredStepTime = Math.floor(1.0 / 30.0 * 1000.0);
        #end

      //BINDING EVENTS
        stage.addEventListener(Event.ACTIVATE, onResume);
        stage.addEventListener(Event.RESIZE, onResize);
        stage.addEventListener(Event.DEACTIVATE, onPause);
        stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
        stage.addEventListener(Event.DEACTIVATE, onExit);

        #if (SHOW_FPS && debug)
            fps = new FPS(10, 10, 0xff0000);
            this.addChild(fps);
        #end
    }

  //MAIN LOOP
    function onEnterFrame(e : Event) {
        #if ( windows && debug && USE_OPENGL)
        ShaderManager.update(SMain.delta);
        #end

        #if (SHOW_FPS && debug)
            var now : Int = Lib.getTimer();
        #end
      //CALCULATING DELTA
        if (prevFrame < 0) prevFrame = Lib.getTimer();
        delta = Lib.getTimer() - prevFrame;
        prevFrame = Lib.getTimer();

        #if USE_NAPE
      //NAPE STEP
        nDelta += Math.floor(Math.min(delta, DEATH_LOOP_MARGIN));
        napeDelta = 0;
        if(FIXED_TIMESTAMP >= 1){
            while(nDelta >= desiredStepTime){
                napeDelta += desiredStepTime;
                space.step(desiredStepTime / 1000.0, VELOC_ITER, POS_ITER);
                nDelta -= desiredStepTime;
            }
            movAlpha = nDelta / desiredStepTime;
        }else {
            napeDelta = delta;
            space.step(Math.max(0.01,delta / 1000), VELOC_ITER, POS_ITER);
            movAlpha = 1;
        }
        #end

        #if SHOW_FPS
            //fps.addSegmentTime(Lib.getTimer() - now, "P");
        #end
    }

  //EVENTS
    function onPause(e : Event) {
        if (screen != null)	screen.pause();
    }

    function onResize(e : Event){
        aspect.fix(null);
        #if !flash
            ShaderManager.reloadAllShaders();
        #end

        for(s in screens)
            s.onResize();
    }

    function onResume(e : Event) {
        if (screen != null)	screen.resume();
    }

    function onExit(e : Event){
        GameState.save();
    }

    #if USE_NAPE
  //NAPE HELPERS
    /**
     * Applies temporal aliasing to float
     **/
    public static function tempAliasing(newVal : Float, prevVal : Float){
        return movAlpha * (newVal) + (1.0 - movAlpha) * prevVal;
    }
    #end

  //GETTERS/SETTERS
    static function get_SCREEN_W() : Float {
        return Lib.current.stage.stageWidth / aspect.scaleFactor;
    }

    static function get_SCREEN_H() : Float {
        return Lib.current.stage.stageHeight / aspect.scaleFactor;
    }

  //INPUT
    public static var fingers : Array<Finger>;

    function onDown(e : Dynamic){
        var finger : Finger = null;
        for(f in fingers)
            if(!f.down){
                finger = f;
                break;
            }
        if(finger == null){
            finger = {x : 0.0, y : 0.0, id : 0, down : false};
            fingers.push(finger);
        }

        finger.down = true;
        finger.id = 0;
        finger.x = e.stageX / SMain.aspect.scaleFactor;
        finger.y = e.stageY / SMain.aspect.scaleFactor;
    }

    function onUp(e : Dynamic){
        for(f in fingers){
            if(f.down && f.id == 0){
                f.down = false;

                return;
            }
        }
    }

    function onMove(e : Dynamic){
        for(f in fingers){
            if(f.down && f.id == 0){

                f.x = (e.stageX - SMain.aspect.offsetX) / SMain.aspect.scaleFactor;
                f.y = (e.stageY - SMain.aspect.offsetY) / SMain.aspect.scaleFactor;
                return;
            }
        }
    }
}